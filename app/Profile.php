<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = "profile";
    protected $fillable = ["jenis_kelamin", "tempat_lahir", "tgl_lahir", "alamat", "bio", "no_telp", "profile_foto", "users_id"];
    public $timestamps = false;

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
