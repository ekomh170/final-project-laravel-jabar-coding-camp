<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Komentar;
use App\Posting;

class KomentarController extends Controller
{
    public function store(Request $request)
    {
        $comment = new Komentar;
        $comment->isi_komentar = $request->get('isi_komentar');
        $comment->user()->associate($request->user());
        $post = Posting::find($request->get('posting_id'));
        $post->comments()->save($comment);
        return back();
    }

    public function replyStore(Request $request)
    {
        $reply = new Komentar();
        $reply->isi_komentar = $request->get('isi_komentar');
        $reply->user()->associate($request->user());
        $reply->parent_id = $request->get('comment_id');
        $post = Posting::find($request->get('posting_id'));

        $post->comments()->save($reply);

        return back();

    }
}
