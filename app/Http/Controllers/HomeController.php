<?php

namespace App\Http\Controllers;

use App\Posting;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Posting::all();
        return view('halaman.home', compact('posts'));
    }
}