<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posting;
use App\User;
use Auth;
use File;

class PostingController extends Controller
{

    public function __construct()
    {
        return $this->middleware('auth');
    }

    public function index()
    {
    $posts = Posting::all();

    return view('post.index', compact('posts'));
    }

    public function create()
    {
        return view('post.post');
    }

    public function store(Request $request)
    {
        
        $request->validate([
            'judul_postingan' => 'required',
            'isi_postingan' => 'required',
            'photo' => 'mimes:jpeg,jpg,png',
        ]);

        $photo = $request->photo;
        $new_photo = time() . ' - ' . $photo->getClientOriginalName();
        $request->photo->move(public_path('photo'), $new_photo);
        $posting = Posting::create([
            "judul_post" => $request["judul_postingan"],
            "isi_post" => $request["isi_postingan"],
            "post_foto" => $new_photo,
            "user_id" => Auth::id()
        ]);


        return redirect('/userpost');
    }

    public function show($id)
    {
    $post = Posting::find($id);

    return view('post.show', compact('post'));
    }

    public function userpost(){
        $user = Auth::id();
        $posts = Posting::where("user_id", "=", $user)->get();
        return view('post.userpost', compact('posts'));
    }

    public function edit($id)
    {
        // $post = DB::table('posting')->where('id', $id)->first();
        $post = Posting::find($id);
        // dd($post);
        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'judul_postingan' => 'required',
            'isi_postingan' => 'required',
            'photo' => 'mimes:jpeg,jpg,png',
        ]);

        $post = Posting::findorfail($id);

        if($request->has('photo')){
            $path = 'photo/';
            File::delete($path . $post->post_foto);
            $foto = $request->photo;
            $new_foto = time() . ' - ' . $foto->getClientOriginalName();
            $foto->move($path, $new_foto);
            $post_data = [
                "judul_post" => $request->judul_postingan,
                "isi_post" => $request->isi_postingan,
                "post_foto" => $new_foto,
                "user_id" => Auth::id()
            ];
        } else {
            $post_data = [
                "judul_post" => $request->judul_postingan,
                "isi_post" => $request->isi_postingan,
                "user_id" => Auth::id()
            ];
        }
        $post->update($post_data);

        return redirect('/userpost')->with('success', 'Data berhasil diedit!');
    }

    public function destroy($id)
    {
        // $query = DB::table('cast')->where('id', $id)->delete();

        // Cast::destroy($id);

        $post = Posting::findorfail($id);
        $post->delete();

        $path = "photo/";
        File::delete($path . $post->post_foto);
        return redirect('/userpost')->with('success', 'Data berhasil dihapus!');
    }

}
