<?php

namespace App\Http\Controllers;

use App\Follow;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class FollowController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_follow = \Illuminate\Support\Facades\Auth::user()->id;

        $follow = User::where('id', '!=', $user_follow)->get();
        return view('follow.index', compact('follow'));
    }

    public function follow($id)
    {
        $user_follow = \Illuminate\Support\Facades\Auth::user()->id;
        $id_user_mengikuti = $id;

        Follow::create([
            "users_id" => $id_user_mengikuti,
            "id_user_mengikuti" => $user_follow,
            "status" => 1,
        ]);

        alert()->success('Berhasil Memfollow User Tersebut', 'Follow');
        return redirect('/follow');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Follow  $follow
     * @return \Illuminate\Http\Response
     */
    public function unfollow($id)
    {
        $user_follow = \Illuminate\Support\Facades\Auth::user()->id;
        $countfollowers = Follow::where('id_user_mengikuti', '=', $user_follow)
            ->where('users_id', $id)
            ->delete();

        alert()->success('Berhasil Unfollow User Tersebut', 'Follow');
        return redirect('/follow');
    }
}