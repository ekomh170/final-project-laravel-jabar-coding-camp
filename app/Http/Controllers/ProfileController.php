<?php

namespace App\Http\Controllers;

use App\Profile;
use App\Follow;
use App\Posting;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // Ubah Form Profile User
    public function index()
    {
        $user = Auth::id();
        $posts = Posting::where("user_id", "=", $user)->get();
        $user_follow = \Illuminate\Support\Facades\Auth::user()->id;
        $countfollowers = Follow::where('id_user_mengikuti', '=', $user_follow)
            ->join('users', 'users.id', '=', 'follow.id_user_mengikuti')
            ->where('status', 1)
            ->get();

        $countfollowerspengikut = Follow::where('users_id', '=', $user_follow)
            ->join('users', 'users.id', '=', 'follow.users_id')
            ->get();

        $profile = Profile::where('users_id', Auth::user()->id)->first();
        return view('profile.index', compact('profile', 'posts', 'countfollowers', 'countfollowerspengikut'));
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    // Show Profile User Yang Sedang Login
    public function show($id)
    {
        $profile = profile::where('users_id', Auth::user()->id)->first();
        return view('profile.show', compact('profile'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Profile  $profile
     * @return \Illuminate\Http\Response
     */
    // Proses Ubah Dari Form Index Profile
    public function update(Request $request, $id)
    {
        $request->validate([
            'jenis_kelamin' => ['required', 'string'],
            'tempat_lahir' => ['required', 'string'],
            'tgl_lahir' => ['required'],
            'alamat' => ['required', 'string'],
            'bio' => ['required', 'string'],
            'no_telp' => ['required'],
        ]);

        $profile = Profile::find($id);

        if ($request->has('profile_foto')) {
            $path = 'img/profile/';
            \Illuminate\Support\Facades\File::delete($path . $profile->profile_foto);
            $gambar = $request['profile_foto'];
            $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
            $gambar->move($path, $new_gambar);


            $profile_data = [
                'jenis_kelamin' => $request['jenis_kelamin'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
                'alamat' => $request['alamat'],
                'bio' => $request['bio'],
                'no_telp' => $request['no_telp'],
                'profile_foto' => $new_gambar,
            ];
        } else {
            $profile_data = [
                'jenis_kelamin' => $request['jenis_kelamin'],
                'tempat_lahir' => $request['tempat_lahir'],
                'tgl_lahir' => $request['tgl_lahir'],
                'alamat' => $request['alamat'],
                'bio' => $request['bio'],
                'no_telp' => $request['no_telp']
            ];
        }

        Profile::whereId($id)->update($profile_data);
        alert()->success('Berhasil Mengubah Profilemu', 'Profile');
        return redirect('profile/');
    }
}