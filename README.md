## Final Project

## Kelompok 13

## Anggota Kelompok

<li>
<ul><b>Nama : </b>Eko Muchamad Haryono</ul>
<ol><b>Email : </b>ekomh13@gmail.com</ol>
<ol><b>Tugas: </b>Membuat Autentikasi, CRUD/Fitur Profile, Fitur Follow dan Membantu  Mengintergrasi UI dengan Database</ol>

<ul><b>Nama : </b>Faisal Dzulfikar</ul>
<ol><b>Email : </b>faisd405@gmail.com</ol>
<ol><b>Tugas: </b>Membuat UI Profile, Halaman Utama, Halaman Postingan</ol>

<ul><b>Nama : </b>Hakim Usmanto</ul>
<ol><b>Email : </b>hakimusmanto8@gmail.com</ol>
<ol>Tugas: membuat CRUD postingan dan komentar</ol>
</li>
<br>

<h2>Tema Project :</h2>
Membuat Sosial Media<br><br>

## ERD Desain

<img src="public/erd/erd_sosial_media.png" alt="Desain_ERD_Sosial_Media_Kelompok_13">

## Link Video Demo

1. https://drive.google.com/drive/folders/1ajkxVKHWxSTGopAUOwqcm3hgHaFS-J4f?usp=sharing<br>
   Note : Untuk Link Video Demo nya menggunakan Google Drive

## Link Deploy

Note Test Web Deploy:

1.  <b>Link Deploy Heroku: </b>http://final-project-laravel-tim13.herokuapp.com/<br>

    <b>untuk akun user : </b><br>
    <b>email :</b> ekomh13@gmail.com,<br>
    <b>password :</b> amano2829<br>

2.  <b>Link Deploy Alternatif:</b> https://tim13.mathcircle.id
