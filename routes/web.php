<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Halaman Utama
Route::get('/', 'HomeController@index')->name('home');
// Halaman Utama

// Route Login
Auth::routes();
// Route Login

Route::group(['middleware' => ['web']], function () {
    // Route Profile
    Route::resource('/profile', 'ProfileController')->only(['index', 'update', 'show']);
    // Route Profile


    // Route Follow
    Route::get('/follow', 'FollowController@index');
    Route::get('/follow/follow/{id}', 'FollowController@follow')->name('follow.follow');
    Route::get('/follow/unfollow/{id}', 'FollowController@unfollow')->name('follow.unfollow');
    // Route Follow

    // route untuk postingan dan komentar
    Route::get('/post/create', 'PostingController@create')->name('post.create');
    Route::post('/post/store', 'PostingController@store')->name('post.store');
    Route::get('/posts', 'PostingController@index')->name('posts');
    Route::get('/post/show/{id}', 'PostingController@show')->name('post.show');
    Route::get('/post/{id}/edit', 'PostingController@edit')->name('post.edit');
    Route::put('/post/{id}', 'PostingController@update')->name('post.update');
    Route::delete('/post/{id}', 'PostingController@destroy')->name('post.destroy');
    Route::post('/comment/store', 'KomentarController@store')->name('comment.add');
    Route::post('/comment/store', 'KomentarController@store')->name('comment.add');
    Route::post('/reply/store', 'KomentarController@replyStore')->name('reply.add');
    Route::get('/userpost', 'PostingController@userpost')->name('userpost.show');
});

// Route::get('/', function () {
//     return view('halaman.home');
// });

// Route::get('/profile', function () {
//     return view('halaman.profile');
// });

// Route::get('/postingan', function () {
//     return view('halaman.postingan');
// });