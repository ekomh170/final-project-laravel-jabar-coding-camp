@extends('layout.master')
@section('title')
<title>Halaman Postingan</title>
@endsection
@section('judul')
    <h4>Halaman Postingan</h4>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
          <div class="row">
            <div class="col">
              <div class="card">
                <div class="card-body">
                  <div class="tab-content">
                    <div class="active tab-pane" id="activity">
                      <!-- Post -->
                      <div class="post clearfix">
                        <div class="user-block">
                          <img class="img-circle img-bordered-sm" src="{{asset('img/profile.jpg')}}" alt="User Image">
                          <span class="username">
                            <a href="#">Sarah Ross</a>
                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                          </span>
                          <span class="description">Sent you a message - 2 days ago</span>
                        </div>
                        <div class="col-sm-6 mw-25">
                          <img class="img-fluid mb-3 " style="max-width: 320px" src="{{asset('img/profile.jpg')}}" alt="Photo">
                        </div>
                        <!-- /.user-block -->
                        <h4>Judul Postingan</h4>
                        <p>
                          Bio : Lorem ipsum represents a long-held tradition for designers,
                          typographers and the like. Some people hate it and argue for
                          its demise, but others ignore the hate as they create awesome
                          tools to help create filler text for everyone from bacon lovers
                          to Charlie Sheen fans.
                        </p>
                      </div>
                      <!-- /.post -->
                    </div>
                  </div>
                  <!-- /.tab-content -->
                </div>
            <!-- /.col -->

          <!-- /.row -->
        </div>
        <!-- /.container-fluid -->
      </section>
      <!-- /.content -->
@endsection
