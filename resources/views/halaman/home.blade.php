@extends('layout.master')
@section('title')
<title>Halaman Utama</title>
@endsection
@section('judul')
<h4>Halaman Utama</h4>
@endsection
@section('content')
<div class="container">
    <div class="row">
        @foreach ($posts as $post)
        <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
                <img src="{{ asset('photo/'.$post->post_foto) }}" class="card-img-top" alt="{{$post->judul_post}}">
                <div class="card-body">
                    <h5 class="card-title">{{ $post->judul_post }}</h5>
                    <p class="card-text"><b>User Pembuat Post </b>: {{$post->user->name}}</p>
                    <div class="row">
                        <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary p-2 col-8">Halaman
                            Postingan</a>
                        {{-- <span class="fa-layers fa-2x offset-md-2">
                            <a href="#"><i class="far fa-heart"></i></a>
                            <span class="fa-layers-counter" style="background:#cc3300">146</span>
                        </span> --}}
                    </div>
                </div>
            </div>
        </div>
        @endforeach

        {{-- <div class="col-lg-4">
            <div class="card" style="width: 18rem;">
                <img src="{{asset('img/profile.jpg')}}" class="card-img-top" alt="...">
        <div class="card-body">
            <h5 class="card-title">Judul Post</h5>
            <p class="card-text">Isi Post - Some quick example text to build on the card title and make
                up the bulk of the card's content.</p>
            <div class="row">
                <a href="postingan" class="btn btn-primary p-2 col-8">Halaman Postingan</a>
                <span class="fa-layers fa-2x offset-md-2">
                    <a href="#"><i class="far fa-heart"></i></a>
                    <span class="fa-layers-counter" style="background:#cc3300">146</span>
                </span>
            </div>
        </div>
    </div>
</div> --}}
<!-- /.col-md-4 -->
</div>
<!-- /.row -->
</div>

@endsection
