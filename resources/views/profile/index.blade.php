@extends('layout.master')
@section('title')
<title>Halaman Profile</title>
@endsection
@section('title')
<title>Halaman Profile</title>
@endsection
@section('judul')
<h4>Halaman Detail Profile</h4>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->

<!-- Main content -->
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-3">

                <!-- Profile Image -->
                <div class="card card-primary card-outline">
                    <div class="card-body box-profile">
                        <div class="text-center">
                            <img class="profile-user-img img-fluid img-circle"
                                src="{{ asset('/img') }}/profile/{{ $profile->profile_foto }}"
                                alt="User profile picture">
                        </div>

                        <h3 class="profile-username text-center"> {{ Auth::user()->name }}</h3>
                        <hr>
                        <h6 class="card-text text-left"><b>Email</b> : {{ Auth::user()->email }}</h6>
                        <h6 class="card-text text-left"><b>Jenis Kelamin</b> : {{ $profile->jenis_kelamin }}</h6>
                        <h6 class="card-text text-left"><b>Tempat Lahir</b> : {{ $profile->tempat_lahir }}</h6>
                        <h6 class="card-text text-left"><b>Tanggal Lahir</b> : {{ $profile->tgl_lahir }}</h6>
                        <h6 class="card-text text-left"><b>Alamat</b> : {{ $profile->alamat }}</h6>
                        <h6 class="card-text text-left"><b>Bio</b> : {{ $profile->bio }}</h6>
                        <h6 class="card-text text-left"><b>Nomer Telpon</b> : {{ $profile->no_telp }}</h6>
                        <hr style="width:75%">
                        <h6 class="card-text text-left"><b>Akun Dibuat</b> : {{ Auth::user()->created_at }}</h6>

                        <ul class="list-group list-group-unbordered mb-3">
                            <li class="list-group-item">
                                <b>Pengikut</b> <a class="float-right">{{ $countfollowers->count() }}</a><br>
                                <b>Mengikuti</b> <a class="float-right">{{ $countfollowerspengikut->count() }}</a>
                            </li>
                        </ul>

                        <a href="{{route('userpost.show')}}" class="btn btn-primary btn-block"><b>Postingan yang sudah
                                dibuat</b></a>
                        <a href="post/create" class="btn btn-primary btn-block"><b>Tambah Postingan</b></a>
                        <a href="../profile/show" class="btn btn-primary btn-block"><b>Edit Profile</b></a>
                    </div>
                    <!-- /.card-body -->
                </div>
                <!-- /.card -->
            </div>
            <div class="col-md-9">
                @foreach($posts as $post)
                <div class="card">
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="active tab-pane" id="activity">
                                <!-- Post -->
                                <div class="post clearfix">
                                    <div class="user-block">
                                        <img class="img-circle img-bordered-sm"
                                            src="{{ asset('/img') }}/profile/{{ $profile->profile_foto }}"
                                            alt="User Image">
                                        <span class="username">
                                            <a href="#">{{ Auth::user()->name }}</a>
                                            <a href="#" class="float-right btn-tool"><i class="fas fa-times"></i></a>
                                        </span>
                                        <span class="description">{{ $post->created_at }} | To Update :
                                            {{ $post->updated_at }}</span>
                                    </div>
                                    <div class="col-sm-6 mw-25">
                                        <img class="img-fluid mb-3 " style="max-width: 320px"
                                            src="{{ asset('/photo') }}/{{ $post->post_foto }}" alt="Photo">
                                    </div>
                                    <!-- /.user-block -->
                                    <h1>{{ $post->judul_post }}</h1>
                                    <p>
                                        {{ $post->isi_post }}
                                    </p>

                                    <a href="{{ route('post.show', $post->id) }}"
                                        class="btn btn-primary btn-block"><b>Postingan Lengkap dan Komentar</b></a>
                                </div>
                                <!-- /.post -->
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div><!-- /.card-body -->
                </div>
                @endforeach

                <!-- /.card -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </div><!-- /.container-fluid -->
</section>
<!-- /.content -->
@endsection
