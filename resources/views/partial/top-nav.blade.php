<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
    <div class="container">
        {{-- HOME --}}
        <a href="{{ url('/') }}" class="navbar-brand">
            <i class="fas fa-home" alt="Home"></i>
            <span class="brand-text font-weight-bold"> Home</span>
        </a>

        <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse"
            aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse order-3" id="navbarCollapse">
            </ul>

            <!-- SEARCH FORM -->
            <form class="form-inline ml-0 ml-md-3">
                <div class="input-group input-group-sm">
                    <input class="form-control form-control-navbar" type="search" placeholder="Search"
                        aria-label="Search">
                    <div class="input-group-append">
                        <button class="btn btn-navbar" type="submit">
                            <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <!-- Right navbar links -->
        <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item">
                    <!-- Add CRUD Postingan -->
                    <a class="nav-link" href="{{ url('post/create') }}">
                        <i class="fas fa-plus-square" alt="Tambah Postingan"></i>
                        <span class="font-weight-bold">Tambah Postingan</span>
                    </a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ url('follow/') }}">Follow Users Terdaftar</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{ url('profile/show') }}">
                        Ubah Profile</a>
                </li>
                <li class="nav-item">
                    <!-- Profile -->
                    <a class="nav-link" href="{{ url('profile') }}">
                        <i class="fas fa-user-alt" alt="Profile"></i>
                        <span class="font-weight-bold">Profileku</span>
                    </a>
                </li>
                <li class="nav-item">
                    <!-- Logout -->
                    <a class="nav-link" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                                                                     document.getElementById('logout-form').submit();">
                        {{ __('Logout') }}

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                        <i class="fas fa-sign-out-alt" alt="Logout"></i>
                    </a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link" href="#">
                        <span class="caret"></b>Welcome, {{ Auth::user()->name }}</span>
                    </a>
                </li>
                @endguest
            </ul>

        </ul>
    </div>
</nav>