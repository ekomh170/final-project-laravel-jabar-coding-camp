@extends('layout.master')
@push('style')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.css" />
@endpush
@section('title')
<title>Halaman Users Terdaftar</title>
@endsection
@section('judul')
<h4>Halaman Users Yang Terdaftar</h4>
@endsection
@section('content')
<section class="content">

    <!-- Default box -->
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">@yield('judul_sub')</h3>
                    </div>
                    <div class="card-body">
                        <div class="h2 mb-3 text-center">Data Follow Users</div>
                        <hr style="width:75%">
                        <table id="table" class="table table-bordered mt-3 text-center">
                            <thead>
                                <tr>
                                    <th style="width: 10px">No</th>
                                    <th>Nama Users</th>
                                    <th>Email</th>
                                    <th>Aksi Follow</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($follow as $key => $data)
                                <tr>
                                    <td>{{ $key + 1 }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>
                                        @php
                                        $cek_follow = App\Follow::where('id_user_mengikuti', '=', $data->id)->first();
                                        $cek_follow_status = App\Follow::where('status', '=', 1)->first();
                                        @endphp
                                        @auth
                                        <a href="{{ url('follow/follow', $data->id) }}"
                                            class="btn btn-outline-danger my-1">Follow User</a> |
                                        <a href="{{ url('follow/unfollow', $data->id) }}"
                                            class="btn btn-outline-danger my-1">UnFollow User</a>
                                        @endauth
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script src="template/plugins/datatables/jquery.dataTables.js"></script>
<script src="adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<script>
    $(function () {
    $("#table").DataTable();
  });
</script>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.2/datatables.min.js"></script>
@endpush
@endsection