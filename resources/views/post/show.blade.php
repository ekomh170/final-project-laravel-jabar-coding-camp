@extends('layout.master')
@section('title')
<title>Halaman Lihat Postingan</title>
@endsection
@section('judul')
<h4>Halaman Lihat Postingan</h4>
@endsection
<style>
    .display-comment .display-comment {
        margin-left: 40px
    }
</style>
@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <p><b>{{ $post->judul_post }}</b></p>
                    <br />
                    <p><img src="{{ asset('photo/'.$post->post_foto) }}" alt="{{$post->judul_post}}">
                    </p>
                    <p>
                        {{ $post->isi_post }}
                    </p>
                    <hr />
                    <h4>Display Comments</h4>
                    @include('partial._comment_replies', ['comments' => $post->comments, 'post_id' => $post->id])
                    <hr />
                    <h4>Add comment</h4>
                    <form method="post" action="{{ route('comment.add') }}">
                        @csrf
                        <div class="form-group">
                            <input type="text" name="isi_komentar" class="form-control" />
                            <input type="hidden" name="posting_id" value="{{ $post->id }}" />
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-warning" value="Add Comment" />
                        </div>
                    </form>

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
