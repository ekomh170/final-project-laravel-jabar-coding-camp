@extends('layout.master')
@section('title')
<title>Daftar Postingan yang Sudah Dibuat</title>
@endsection
@section('judul')
<h4>Daftar Postingan yang Sudah Dibuat</h4>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <table class="table table-striped">
                <thead>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Action</th>
                </thead>
                <tbody>
                @foreach($posts as $post)
                <tr>
                    <td>{{ $post->id }}</td>
                    <td>{{ $post->judul_post }}</td>
                    <td>
                        <a href="{{ route('post.show', $post->id) }}" class="btn btn-primary">Show Post</a>
                        <a href="{{ route('post.edit', $post->id) }}" class="btn btn-warning">Edit Post</a>
                        <form action="{{ route('post.destroy', ['id' => $post->id]) }}" method="POST">
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>

                    </td>
                </tr>
                @endforeach
                </tbody>

            </table>
        </div>
    </div>
</div>
@endsection