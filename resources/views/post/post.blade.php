@extends('layout.master')
@section('title')
<title>Halaman Create Postingan</title>
@endsection
@section('judul')
<h4>Halaman Create Postingan</h4>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Buat Postingan Baru</div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="{{ route('post.store') }}">
                        <div class="form-group">
                            @csrf
                            <label class="label">Judul Postingan: </label>
                            <input type="text" name="judul_postingan" class="form-control" required/>
                            @error('judul_postingan')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror

                        </div>
                        <div class="form-group">
                            <label class="label">Isi Postingan: </label>
                            <textarea name="isi_postingan" rows="10" cols="30" class="form-control" required></textarea>
                            @error('isi_postingan')
                                <div class="alert alert-danger">
                                    {{ $message }}
                                </div>
                            @enderror
                        </div>
                        <div class="form-group">
                        <div class="form-group">
                               <label for="photo">Tambahkan Foto</label>
                               <input type="file" class="form-control" name="photo" id="photo" placeholder="Silakan tambahkan foto untuk postingan!">
                               @error('photo')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                               @enderror
                        </div>
                
                            <input type="submit" class="btn btn-success" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection