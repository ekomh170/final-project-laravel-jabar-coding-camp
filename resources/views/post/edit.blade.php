@extends('layout.master')
@section('title')
<title>Halaman Edit Postingan</title>
@endsection
@section('judul')
<h4>Halaman Edit Postingan</h4>
@endsection
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Postingan</div>
                <div class="card-body">
                    <form method="post" enctype="multipart/form-data" action="/post/{{$post->id}}">
                        <div class="form-group">
                            @csrf
                            @method('put')
                            <label class="label">Judul Postingan: </label>
                            <input type="text" name="judul_postingan" value="{{$post->judul_post}}" class="form-control" required/>
                        </div>
                        <div class="form-group">
                            <label class="label">Isi Postingan: </label>
                            <textarea name="isi_postingan" rows="10" cols="30" class="form-control" required>{{$post->isi_post}}</textarea>
                        </div>
                        <div class="form-group">
                        <div class="form-group">
                               <label for="photo">Tambahkan Foto</label>
                               <input type="file" class="form-control" name="photo" id="photo" placeholder="Silakan tambahkan foto untuk postingan!">
                               @error('photo')
                                    <div class="alert alert-danger">
                                        {{ $message }}
                                    </div>
                               @enderror
                        </div>
                
                            <input type="submit" class="btn btn-success" />
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection